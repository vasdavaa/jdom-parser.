
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DOMModify {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Kérem a fájl nevét!");
		String fileName = input.nextLine();
        
		File xmlFile = new File(fileName);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
        	//XML fájl megnyitása
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            //Módosítások elvégzése
            update(doc);
            //XML fájl írása
            writeXMLFile(doc, fileName);
            
        } catch (SAXException | ParserConfigurationException | IOException | TransformerException e1) {
        	e1.printStackTrace();
            System.out.println("A fájl megnyitása, nem sikerült");
            System.exit(0);
        }
       
	}
	
	private static void writeXMLFile(Document doc,String fileName)							
		throws TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException {
	        doc.getDocumentElement().normalize();
	        TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        
	        /* Kapott fájlnevet a "." karakternél elvágom, 
	         * és az első részéhez(azaz  a kiterjesztés nélküli fájlnévhez),
	         * hozzáfűzöm a következő Stringet "_UPDATED.xml". 
	         * Így nem az eredeti fájlt írjuk felül,
	         * és ellenörizhető a kimenet.
	         */
	        
	        fileName = fileName.split("\\.",2)[0].concat("_UPDATED.xml");
	        StreamResult result = new StreamResult(new File(fileName));		
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.transform(source, result);
	        System.out.println("Új XML fájl sikeresen elkészült, "+fileName+" néven."); 
	    }
			
	private static void update(Document doc) {
		Scanner input = new Scanner(System.in);
    	NodeList root = (NodeList) doc.getDocumentElement();
    	System.out.println("A fájl beolvasása, sikeres volt");
    	System.out.println("Adja meg a frissíteni kívánt, node nevét!");
    	String nodeName = input.nextLine();	
    	List<Node> nodeList = new ArrayList<Node>();
    	getNodes((Node)doc.getDocumentElement(),nodeName,nodeList);
	    for(int i=0;i<nodeList.size();i++)
	    	System.out.println((i+1)+" - "+nodeList.get(i).getTextContent());
	    System.out.println("Adja meg a node sorszámát amelyiket frissíteni szeretné vagy nyomjon enter-t az összes módosításához");
	    String option = input.nextLine();
	    System.out.println("Kerem adja meg az új értéket!");
    	String newValue = input.nextLine();	
	    if(option.equals("")) 
	    	for(int i=0;i<nodeList.size();i++)
		    	nodeList.get(i).setTextContent(newValue);
	    else 
	    	nodeList.get(Integer.parseInt(option)-1).setTextContent(newValue);
	}		
	/*
	 * Összeszedi a megadott névvel rendelkező node-okat, egy listába.
	 * A node-ok keresését rekurzívan végzi.
	 */
	private static void getNodes(Node node,String name,List nodeList) {
    	if(node.getNodeName().equals(name)) {
    		nodeList.add(node);
    	}
	    NodeList childrenNodes = node.getChildNodes();
	    for (int i = 0; i < childrenNodes.getLength(); i++) {	
	        Node currentnode = childrenNodes.item(i);			
	        if (currentnode.hasChildNodes()) 				
	        	getNodes(currentnode,name,nodeList);
	    }
	}	    	

}
