
import java.io.IOException;

import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DOMRead {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Kérem a fájl nevét!");
		String filename = input.nextLine();
		printXML(filename);
	}
	
	public static void printXML(String file) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			//A fájl megnyitása
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(file);	
			//Itt megkaptam a gyökérelemet
			NodeList root = (NodeList) doc.getDocumentElement();
			System.out.println(doc.getDocumentElement().getNodeName()+":");
			String tab = "\t";
			//Gyökérelem node-jain végig iterálok és rekurzívan kiírom őket.
			for(int i=0;i<root.getLength();i++)
				printNode(root.item(i),tab);
		}
		catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			System.out.println("Fájl beolvasás nem sikerült");
		}
	}
	/*
	 * Node-ok történő iterálás, rekurzívan.
	 * 
	 * Kíirja a node nevét, illetve a hozzátartozó attribútumokat.
	 * Megnézi hogy az aktuális node-nak, vannak-e gyerekei.
	 * Ha igen, vannak:
	 * 		Listába szedi őket és meghívja újra a függvényt, egyesével az összes gyerekére.
	 * Ha nincsenek neki:
	 * 		Kiírja a node-ban található értéket(Mivel itt feltételezem, hogy amennyiben nincs gyereke, valószínűleg rendelkezik valamilyen értékkel.  
	 */
	private static void printNode(Node node,String tab) {
		if (node.getNodeType() == Node.ELEMENT_NODE) System.out.print(tab + node.getNodeName()+":"+ (node.hasAttributes() ? printAttributes(node.getAttributes()) :  "" ) );
	    NodeList nodeList = node.getChildNodes();
	    tab+="\t";
	    for (int i = 0; i < nodeList.getLength(); i++) {	
	        Node currentnode = nodeList.item(i);			
	        if (currentnode.hasChildNodes()) 				
	        	printNode(currentnode,tab);
	        else System.out.print("\t"+currentnode.getTextContent());
	    }
	    
	}
	/* Attribútumok összefűzése egy Stringbe.
	 * Végig iterál az aktuális attribútum listán és egyszerűen hozzáfűzi egy Stringhez az értékeket, majd ezzel visszatér.
	 */
	public static String printAttributes(NamedNodeMap attributeList) {
		String attributes=" [";
		for(int j=0;j<attributeList.getLength();j++) {
			Node attribute = attributeList.item(j);				
			attributes+=(" "+attribute.getNodeName()+"=\""+attribute.getNodeValue()+"\" ");
		}
		attributes+="]";
		return attributes;
	}
}
